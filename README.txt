Apple Push Notifications for Drupal
-----------------------------------
Author: lawik

The APN module for Drupal allows sending of Push notifications to users for iOS
app backend systems. It handles sending messages through the fabulous Apns-PHP
library which we sadly might not be allowed to include because of it's non-GPL
BSD license.

This is a technical solution, it does not have much front-end except for
critical configuration. You need to understand how to use a web service, you
need to know how to create an iOS app already. Let us know what kind of
additions would improve this module for you.

Requirements:
--------------------------------------------------------------------------------

* Services 2.x
  The fairly stable solution. Has been used with both XML-RPC and JSON Server.
* OR Services 3.x
  The entirely UNTESTED solution. Please let us know how it works.
* An iOS app and an Apple Developer account.
* The Entrust CA certificate. I don't own the cert so I won't include it. It can
  be found through the instructions for ApnsPHP. We used as file like
  entrust_root_certification_authority.pem in the module directory.
* ApnsPHP - The library is found at http://code.google.com/p/apns-php and should
  be unpacked in the ApnsPHP directory in the module dir so the module can find
  Autoload.php.

Usage:
--------------------------------------------------------------------------------

Short about Apple Push Notifications:

To send a Push Notification the server needs a specific key which identifies the
specific device + app combination to know which device it's targeting.
This module allows registering of this push key through Drupal-based web
services. You register the key with a user account.

To generate this key your iOS app needs to do something like this:
http://code.google.com/p/apns-php/source/browse/trunk/Objective-C%20Demo OR
http://code.google.com/p/apns-php/wiki/ObjectiveC (deprecated)

You need a certificate for your app:
http://code.google.com/p/apns-php/wiki/CertificateCreation

You also need a CA cert.
The Entrust Root Certification Authority .pem-file.
See the ApnsPHP instructions.

You also need your own certificate for sending. It can be configured under:
Admin - Site Configuration - Apple Push Notifications

These is a test-sending facility once you've registered at least one key at:
admin/settings/apn/send (Admin - Site Configuration - APN - Send)

To actually implement this with your module you will need a custom module at the
moment.
We will likely implement Rules support for this module in a future release.

Special notes:
--------------------------------------------------------------------------------

! Anonymous user push keys are possible but entirely untested at the moment. And
  restricts usage of the key somewhat, enable/disable push for the user is not
  possible.

! There is no admin interface for push key management. If you need to, it's the
  database for now. Views integration might be an option here later or at least
  some means of handling the keys.

! Feedback-handling (disabling dead tokens) is fairly untested so far.

! When sending to multiple users in one go, use the apn_create_push_object and
  apn_push_add_message to reduce the number of connections to the Apple servers.
